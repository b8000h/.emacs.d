;(setenv "HOME" "C:/emacs") ; 主要是为了 .emacs ，是 ~

; TODO: 如何判断所在OS，以此来使用不同的配置？
;(setq shell-file-name "C:\Users\Administrator\.babun\cygwin\bin\zsh.exe")
;(setenv "PATH" (concat (getenv "PATH") ";C:/GUNTools/emacs-23.2/")) ; 主要是为了 M-x shell 运行外部程序


'(load-file "~/.emacsrc/mapping.el")
'(load-file "~/.emacsrc/frames.el")
'(load-file "~/.emacsrc/plugin_ecb.el")
'(load-file "~/.emacsrc/plugin_speedbar.el")

;;; 括号匹配时显示另一个括号而不是跳到另一个括号
(show-paren-mode t)
(setq show-paren-style 'parentheses)

(setq auto-save-default nil) ;不生成名为#filename#的临时文件

(setq x-select-enable-clipboard t) ;支持和外部程序的拷贝

(global-font-lock-mode t) ;打开语法高亮

(setq auto-image-file-mode t) ;让Emacs可以直接打开、显示图片

;; 改变Emacs要你回答yes的行为,按y或空格键表示yes，n表示no。
(fset 'yes-or-no-p 'y-or-n-p)

;;设置打开文件的缺省路径，这里为桌面，默认的路径为“～/”
;(setq default-directory "~/")
;(setq default-directory "～/桌面")
 
