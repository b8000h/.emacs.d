;(add-to-list 'load-path "~/.emacs.d/yourplugin") 插件解压好后这么做加入路径
;(require 'yourplugin) 再这么做开启插件

(add-to-list 'load-path "~/.emacs.d/ecb")
(require 'ecb) ; 开启
(ecb 1) ; 随 Emacs 启动加载

;(global-set-key (kbd "<f7>") 'ecb-minor-mode)   ; 打开ejb
(setq ecb-tip-of-the-day nil) ;;不显示每日的提醒功能                                           
(global-set-key [f12] 'ecb-activate)
(global-set-key [C-f12] 'ecb-deactivate)

 ;;;;show&hide window  
(global-set-key [C-f1] 'ecb-hide-ecb-windows)  
(global-set-key [C-f2] 'ecb-show-ecb-windows) 

;;;;ejb 快捷键
(global-set-key (kbd "C-<left>") 'windmove-left)   ;左边窗口
(global-set-key (kbd "C-<right>") 'windmove-right)  ;右边窗口
(global-set-key (kbd "C-<up>") 'windmove-up)     ; 上边窗口
(global-set-key (kbd "C-<down>") 'windmove-down)   ; 下边窗口
  
;; ;;;; 使某一ecb窗口最大化  
(global-set-key (kbd "C-c 1") 'ecb-maximize-window-directories)  
(global-set-key (kbd "C-c 2") 'ecb-maximize-window-sources)  
(global-set-key (kbd "C-c 3") 'ecb-maximize-window-methods)  
(global-set-key (kbd "C-c 4") 'ecb-maximize-window-history)  
  
;; ;;;;恢复原始窗口布局  
(global-set-key (kbd "C-c 0") 'ecb-restore-default-window-sizes)
