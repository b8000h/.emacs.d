
;; 全屏设置 
(global-set-key [f11] ‘my-fullscreen) 
(defun my-fullscreen () 
(interactive) 
(x-send-client-message nil 0 nil “_NET_WM_STATE” 32 ‘(2 “_NET_WM_STATE_FULLSCREEN” 0)) 
)

;; 最大化设置 
(global-set-key [f12] ‘my-maximized) 
(defun my-maximized () 
(interactive) 
(x-send-client-message nil 0 nil “_NET_WM_STATE” 32 ‘(2 “_NET_WM_STATE_MAXIMIZED_HORZ” 0)) (x-send-client-message nil 0 nil “_NET_WM_STATE” 32 ‘(2 “_NET_WM_STATE_MAXIMIZED_VERT” 0)) 
)

;; 启动就将 Frame 最大化(根据你自己的显示器的分辨率做相应调整) 
;(setq initial-frame-alist ‘((top . 10)(left . 10)(width . 155)(height . 35))) 
;; 设置启动时窗口的长宽，下面为80*40 
;(setq initial-frame-alist ‘((width . 80) (height . 40)))

;; 设置 Frame 的标题 
;(setq frame-title-format 
;(concat “%b - emacs@” (system-name))) 
(setq frame-title-format "Vincent@%b")
;(setq frame-title-format “Welcome to Emacs world! “)

;; 取消菜单栏，F10 开启关闭菜单 
(menu-bar-mode nil) 
;; 取消工具栏 
(tool-bar-mode nil) 
;; 取消滚动栏 
(set-scroll-bar-mode nil)

;; emacs23 开始已内置 linum 以显示行号 
(global-linum-mode t)

;;关闭emacs启动时的画面 
(setq inhibit-startup-message t) 
;;关闭gnus启动时的画面 
(setq gnus-inhibit-startup-message t)

;; 在 Mode Line 显示时间 
(display-time) 
(display-time-mode t) 
;; 使用24小时制 
(setq display-time-24hr-format t)
