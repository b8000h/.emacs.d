## Linux 下，
用户首要配置文件为 ~/.emacs

用户次要配置文件为 ~/.emacs.d/init.el
```
git clone https://github.com/b8000h/.emacs.d.git ~/.emacs.d
```
为了方便版本控制，删除 ~/.emacs，使用 ~/.emacs.d/init.el（受 purcell 的启发）

## Windows 下
+ Windows 10 下，通常是 C:/Documents and Settings/Administrator/Application Data/.emacs
+ Windows 7 下，通常是 C:/Users/Administrator/AppData/Roaming/.emacs
+ Windows XP 下，通常是 C:/Documents and Settings/Application Data/Administrator/.emacs

刚安装好的 Emacs，`.emacs` 是没有建立的，需要自己手动建立


Windows下无法在外部手动建立，可以在Emacs中执行 C-x C-f 来新建。

## 通过在 `.emacs` 中引进外部文件，转移配置文件到一个你更方便的位置，如
```
'(load-file "C:/emacs/.emacs")
```
