
;; 把C-j绑定到到达指定行上
(global-set-key (kbd "C-j") 'goto-line)
 
;; 设置C->键作为窗口之间的切换，默认的是C-x-o,比较麻烦
(global-set-key (kbd "C->") 'other-window)
 
;; 设置C-/为undo,M-/为set-mark
(global-set-key (kbd "C-/") 'undo)
 
;; 设置M-/作为标志位，默认C-@来setmark,C-@不太好用
;; M-/本来对应zap-to-char，这里占用了 (global-set-key (kbd "M-/") 'set-mark-command)
;; 扩大或者缩小窗口（上下）,Ctrl+{}
(global-set-key (kbd "C-}") 'enlarge-window)
(global-set-key (kbd "C-{") 'shrink-window)
