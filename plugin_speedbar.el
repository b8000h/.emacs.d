;(add-to-list 'load-path "~/.emacs.d/yourplugin") 插件解压好后这么做加入路径
;(require 'yourplugin) 再这么做开启插件

;; speedbar 是 Emacs 内置的
(speedbar 1) ;;让speedbar随Emacs一起启动

;; sr-speedbar
;(require 'sr-speedbar);;这句话是必须的
;(add-hook 'after-init-hook '(lambda () (sr-speedbar-toggle)));;开启程序即启用
